# Kinematic Model Optimizer

A place for experiments on the kinematic model optimizer part of our hand tracking. The useful parts of this have been upstreamed into Monado, so this is mostly for reference.